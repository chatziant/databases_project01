import psycopg2
from config import config

def create():
    commands=(
    """
    CREATE TABLE IF NOT EXISTS report(
        ID SERIAL PRIMARY KEY,
        create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        status VARCHAR(50) NOT NULL,
        complete_date TIMESTAMP DEFAULT NULL,
        service_req_type VARCHAR(100) NOT NULL,
        street VARCHAR(100) DEFAULT NULL,
        ZIP INTEGER DEFAULT NULL,
        X DOUBLE PRECISION DEFAULT NULL,
        Y DOUBLE PRECISION DEFAULT NULL,
        ward INTEGER DEFAULT NULL,
        police_district INTEGER DEFAULT NULL,
        community_area INTEGER DEFAULT NULL,
        SSA INTEGER DEFAULT NULL,
        latitude DOUBLE PRECISION DEFAULT NULL,
        longitude DOUBLE PRECISION DEFAULT NULL,
        location VARCHAR(100) DEFAULT NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS reportsub(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        userID INTEGER DEFAULT NULL,
        username VARCHAR(100) DEFAULT NULL,
        subdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS queriessub(
        ID SERIAL PRIMARY KEY,
        userID INTEGER DEFAULT NULL,
        username VARCHAR(100) DEFAULT NULL,
        subdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP
    );
    """

    """
    CREATE TABLE IF NOT EXISTS abandoned_vehicles(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        licence_plate VARCHAR(500) DEFAULT NULL,
        model VARCHAR(100) DEFAULT NULL,
        color VARCHAR(100)  DEFAULT NULL,
        days_parked NUMERIC DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS garbage_carts(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        carts_delivered NUMERIC DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS graffiti_removal(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        surface_type VARCHAR(100) DEFAULT NULL,
        location VARCHAR(100) DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS pot_holes(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        potholes_filled NUMERIC DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS rodent_baitings(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        premises_baited NUMERIC DEFAULT NULL,
        premises_with_garbage NUMERIC DEFAULT NULL,
        premises_with_rats NUMERIC DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS sanitation_complaints(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        violation_nature VARCHAR(100) DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS trees(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        location VARCHAR(100) DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE TABLE IF NOT EXISTS activity(
        ID SERIAL PRIMARY KEY,
        repID INTEGER DEFAULT NULL,
        curr_activity VARCHAR(100) DEFAULT NULL,
        most_recent_action VARCHAR(100) DEFAULT NULL,
        FOREIGN KEY (repID) REFERENCES report(ID) ON UPDATE CASCADE ON DELETE SET NULL
    );
    """,

    """
    CREATE OR REPLACE FUNCTION func01(start_date TIMESTAMP, end_date TIMESTAMP)
    RETURNS TABLE(request_type VARCHAR(100), total_requests BIGINT)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT service_req_type, count(ID) AS total
      FROM report
      WHERE create_date>=start_date AND create_date<=end_date
      GROUP BY service_req_type
      ORDER BY total DESC;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func02(type VARCHAR(100), start_date TIMESTAMP, end_date TIMESTAMP)
    RETURNS TABLE(day TIMESTAMP, total_requests BIGINT)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT create_date, count(ID)
      FROM report
      WHERE service_req_type=type AND create_date>=start_date AND create_date<=end_date
      GROUP BY create_date
      ORDER BY create_date ASC;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func03(day TIMESTAMP)
    RETURNS TABLE(ZIP INTEGER, request_type VARCHAR(100))
    AS $$
    BEGIN
      CREATE TEMP TABLE IF NOT EXISTS temp_table AS
      SELECT report.ZIP AS codeZIP,
             report.service_req_type AS type,
             count(report.ID) AS freq
      FROM report
      WHERE report.create_date = day
      GROUP BY report.ZIP, report.service_req_type;

      RETURN QUERY
      SELECT t1.ZIPcode, temp_table.type
      FROM
      (
        SELECT temp_table.codeZIP AS ZIPcode, MAX(temp_table.freq) max_freq
        FROM temp_table
        GROUP BY ZIPcode
      ) AS t1 INNER JOIN temp_table ON t1.ZIPcode=temp_table.codeZIP
                                     AND t1.max_freq = temp_table.freq;

      DROP TABLE temp_table;

    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func04(start_date TIMESTAMP, end_date TIMESTAMP)
    RETURNS TABLE(request_type VARCHAR(100), avg_time_diff INTERVAL)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT t1.service_req_type AS type, AVG(t1.time_diff) AS lapse
      FROM
      (
        SELECT report.service_req_type,
               report.create_date,
               report.complete_date,
               AGE(report.complete_date,report.create_date) AS time_diff
        FROM report
        WHERE create_date>=start_date AND create_date<=end_date
      ) AS t1
      GROUP BY type;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func05(lat1 DOUBLE PRECISION, lat2 DOUBLE PRECISION, lon1 DOUBLE PRECISION, lon2 DOUBLE PRECISION, day TIMESTAMP, OUT request_type VARCHAR(100))
    AS $$
    BEGIN
      CREATE TEMP TABLE IF NOT EXISTS temp_table1 AS
      SELECT report.service_req_type AS serv_type, COUNT(report.ID) AS freq
      FROM report
      WHERE (report.create_date = day)
       AND (report.latitude BETWEEN lat1 AND lat2)
       AND (report.longitude BETWEEN lon1 AND lon2)
      GROUP BY report.service_req_type;

      SELECT INTO request_type temp_table1.serv_type
      FROM temp_table1
      ORDER BY temp_table1.freq DESC LIMIT 1;

      DROP TABLE temp_table1;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func06(start_date TIMESTAMP, end_date TIMESTAMP)
    RETURNS TABLE(SSA INTEGER, total_requests BIGINT)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT report.SSA, COUNT(report.ID) AS total
      FROM report
      WHERE report.service_req_type IN ('Abandoned Vehicle Complaint',
                                        'Garbage Carts',
                                        'Graffiti Removal',
                                        'Pot Holes Reported')
        AND report.create_date>=start_date AND report.create_date<=end_date
        AND report.SSA IS NOT NULL
      GROUP BY report.SSA
      ORDER BY total DESC LIMIT 5;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func07()
    RETURNS TABLE(lic_plate VARCHAR(500))
    AS $$
    BEGIN
      RETURN QUERY
      SELECT t.licence_plate
      FROM(
        SELECT licence_plate, COUNT(ID) AS freq
        FROM abandoned_vehicles
        WHERE licence_plate IS NOT NULL
        GROUP BY licence_plate
        HAVING COUNT(ID)>1
      ) AS t
      ORDER BY licence_plate ASC;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func08(OUT second_most_common_color VARCHAR(100))
    AS $$
    BEGIN
      SELECT INTO second_most_common_color t.color
      FROM
      (
        SELECT color,
               COUNT(abandoned_vehicles.ID) AS freq,
               ROW_NUMBER () OVER (ORDER BY COUNT(abandoned_vehicles.ID) DESC) AS row
        FROM abandoned_vehicles
        WHERE color IS NOT NULL
        GROUP BY color
        ORDER BY freq DESC
      ) AS t
      WHERE t.row=2;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func09(num BIGINT)
    RETURNS TABLE(request_ID INTEGER)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT repID
      FROM rodent_baitings
      WHERE premises_baited IS NOT NULL AND premises_baited<num;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func10(num BIGINT)
    RETURNS TABLE(request_ID INTEGER)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT repID
      FROM rodent_baitings
      WHERE premises_with_garbage IS NOT NULL AND premises_with_garbage<num;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func11(num BIGINT)
    RETURNS TABLE(request_ID INTEGER)
    AS $$
    BEGIN
      RETURN QUERY
      SELECT repID
      FROM rodent_baitings
      WHERE premises_with_rats IS NOT NULL AND premises_with_rats<num;
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE OR REPLACE FUNCTION func12(day TIMESTAMP)
    RETURNS TABLE(police_district INTEGER)
    AS $$
    BEGIN
      RETURN QUERY
      (SELECT report.police_district
      FROM report INNER JOIN pot_holes ON report.ID=pot_holes.repID
      WHERE pot_holes.potholes_filled>1 AND report.create_date=day)
      UNION
      (SELECT report.police_district
      FROM report INNER JOIN rodent_baitings ON report.ID=rodent_baitings.repID
      WHERE rodent_baitings.premises_baited>1 AND report.create_date=day);
    END; $$
    LANGUAGE PLPGSQL;
    """,

    """
    CREATE INDEX create_date_idx ON report(create_date);
    """,
    """
    CREATE INDEX service_type_idx ON report(service_req_type);
    """,
    """
    CREATE INDEX ZIP_idx ON report(ZIP);
    """,
    """
    CREATE INDEX latitude_idx ON report(latitude);
    """,
    """
    CREATE INDEX longitude_idx ON report(longitude);
    """,
    """
    CREATE INDEX color_idx ON abandoned_vehicles(color);
    """,
    """
    CREATE INDEX potholes_idx ON pot_holes(potholes_filled);
    """
    )

    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = config()
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params)
        # create a cursor
        cur = conn.cursor()

        print('Database creation starts...')
        for com in commands:
            cur.execute(com)

        cur.close()
        conn.commit()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
