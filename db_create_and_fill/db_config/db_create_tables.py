import sys
sys.path.insert(0,'../db_insert')
from create import create
from db_insert_data import db_insert_data

if __name__ == '__main__':
    print('Data base table creation starts...')
    create()
    print('Data base table creation finished!')

    print('Data base data insertion starts...')
    db_insert_data()
    print('Data base creation data insertion finished!')

    print('All done!')
