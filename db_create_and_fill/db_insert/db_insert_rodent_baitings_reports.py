import sys
sys.path.insert(0,'../db_config')
import csv
import psycopg2
from config import config

def db_insert_rodent_baitings_reports():
    path='../../chicago-311-service-requests/311-service-requests-rodent-baiting.csv'

    in_sql1 = "INSERT INTO report(create_date, status, complete_date, service_req_type, street, ZIP, X, Y, ward, police_district, community_area, SSA, latitude, longitude, location) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING ID;"
    in_sql3 = "INSERT INTO rodent_baitings(repID, premises_baited, premises_with_garbage, premises_with_rats) VALUES(%s, %s, %s, %s);"
    in_sql4 = "INSERT INTO activity(repID, curr_activity, most_recent_action) VALUES(%s, %s, %s);"

    conn = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()

        print('Inserting rodent baitings reports data')
        # execute the INSERT statement(s)
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',')
            names = reader.fieldnames

            for row in reader:
                for i in names:
                    if(row[i]==''):
                        row[i]=None

                t1=(row[names[0]],row[names[1]],row[names[2]],
                'Rodent Baiting', row[names[10]], row[names[11]], row[names[12]],
                row[names[13]], row[names[14]], row[names[15]], row[names[16]],
                None, row[names[17]], row[names[18]], row[names[19]])
                cur.execute(in_sql1, t1)

                rep_id = cur.fetchone()[0]

                t3=(rep_id, row[names[5]], row[names[6]], row[names[7]])
                cur.execute(in_sql3, t3)

                t4=(rep_id, row[names[8]], row[names[9]])
                cur.execute(in_sql4, t4)

        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
