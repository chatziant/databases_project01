import sys
sys.path.insert(0,'../db_config')
import psycopg2
from config import config
from db_insert_garbage_reports import db_insert_garbage_reports
from db_insert_vehicle_reports import db_insert_vehicle_reports
from db_insert_graffiti_reports import db_insert_graffiti_reports
from db_insert_pot_holes_reports import db_insert_pot_holes_reports
from db_insert_rodent_baitings_reports import db_insert_rodent_baitings_reports
from db_insert_sanitation_code_reports import db_insert_sanitation_code_reports
from db_insert_street_lights_all_out_reports import db_insert_street_lights_all_out_reports
from db_insert_street_lights_one_out_reports import db_insert_street_lights_one_out_reports
from db_insert_tree_debris_reports import db_insert_tree_debris_reports
from db_insert_tree_trims_reports import db_insert_tree_trims_reports
from db_insert_alley_lights_reports import db_insert_alley_lights_reports

def db_insert_data():
    db_insert_alley_lights_reports()
    db_insert_garbage_reports()
    db_insert_vehicle_reports()
    db_insert_graffiti_reports()
    db_insert_pot_holes_reports()
    db_insert_rodent_baitings_reports()
    db_insert_sanitation_code_reports()
    db_insert_street_lights_all_out_reports()
    db_insert_street_lights_one_out_reports()
    db_insert_tree_debris_reports()
    db_insert_tree_trims_reports()
