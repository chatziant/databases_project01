import sys
sys.path.insert(0,'../db_config')
import csv
import psycopg2
from config import config

def db_insert_users():
    in_sql = "INSERT INTO users(fname, lname, uname, password, address, email) VALUES(%s, %s, %s, %s, %s, %s);"

    conn = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()

        print('Inserting users')
        # execute the INSERT statement(s)
        for i in range(0,10000):
            t=('John'+str(i), 'Doe'+str(i), 'dummy_user'+str(i), 'foo', 'Foo Road Av.', 'jd'+str(i)+'@mail.com')
            cur.execute(in_sql, t)

        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
