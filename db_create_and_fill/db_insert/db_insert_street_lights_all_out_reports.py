import sys
sys.path.insert(0,'../db_config')
import csv
import psycopg2
from config import config

def db_insert_street_lights_all_out_reports():
    path='../../chicago-311-service-requests/311-service-requests-street-lights-all-out.csv'

    in_sql1 = "INSERT INTO report(create_date, status, complete_date, service_req_type, street, ZIP, X, Y, ward, police_district, community_area, SSA, latitude, longitude, location) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"

    conn = None
    try:
        # read database configuration
        params = config()
        # connect to the PostgreSQL database
        conn = psycopg2.connect(**params)
        # create a new cursor
        cur = conn.cursor()

        print('Inserting street lights all out data')
        # execute the INSERT statement(s)
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile, delimiter=',')
            names = reader.fieldnames

            for row in reader:
                for i in names:
                    if(row[i]==''):
                        row[i]=None

                t1=(row[names[0]],row[names[1]],row[names[2]],
                'Lights Out All', row[names[5]], row[names[6]], row[names[7]],
                row[names[8]], row[names[9]], row[names[10]], row[names[11]],
                None, row[names[12]], row[names[13]], row[names[14]])
                cur.execute(in_sql1, t1)

        # commit the changes to the database
        conn.commit()
        # close communication with the database
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
