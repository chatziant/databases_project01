from django.db import models
from django.db import connection
from django.contrib.auth.models import User
import datetime

def function01(start_date, end_date):
    cur = connection.cursor()
    cur.callproc('func01',[start_date, end_date])
    results = cur.fetchall()
    cur.close()

    return results

def function02(service_type, start_date, end_date):
    cur = connection.cursor()
    cur.callproc('func02',[service_type, start_date, end_date])
    results = cur.fetchall()
    cur.close()

    return results

def function03(day):
    cur = connection.cursor()
    cur.callproc('func03',[day])
    results = cur.fetchall()
    cur.close()

    return results

def function04(start_date, end_date):
    cur = connection.cursor()
    cur.callproc('func04',[start_date, end_date])
    results = cur.fetchall()
    cur.close()

    return results

def function05(lat1, lat2, lon1, lon2, day):
    cur = connection.cursor()
    cur.callproc('func05',[lat1, lat2, lon1, lon2, day])
    results = cur.fetchall()
    cur.close()

    return results

def function06(start_date, end_date):
    cur = connection.cursor()
    cur.callproc('func06',[start_date, end_date])
    results = cur.fetchall()
    cur.close()

    return results

def function07():
    cur = connection.cursor()
    cur.callproc('func07',[])
    results = cur.fetchall()
    cur.close()

    return results

def function08():
    cur = connection.cursor()
    cur.callproc('func08',[])
    results = cur.fetchall()
    cur.close()

    return results

def function09(num):
    cur = connection.cursor()
    cur.callproc('func09',[num])
    results = cur.fetchall()
    cur.close()

    return results

def function10(num):
    cur = connection.cursor()
    cur.callproc('func09',[num])
    results = cur.fetchall()
    cur.close()

    return results

def function11(num):
    cur = connection.cursor()
    cur.callproc('func09',[num])
    results = cur.fetchall()
    cur.close()

    return results

def function12(day):
    cur = connection.cursor()
    cur.callproc('func12',[day])
    results = cur.fetchall()
    cur.close()

    return results

# Create your models here.
class Report(models.Model):
    STATUS_TYPE = (('Open','Open'),('Open - Dup','Open - Dup'),('Completed','Completed'),('Completed - Dup','Completed - Dup'))

    SERVICE_TYPE = (('Abandoned Vehicle Complaint','Abandoned Vehicle Complaint'),('Alley Lights Out','Alley Lights Out'),('Garbage Carts','Garbage Carts'),('Graffiti Removal','Graffiti Removal'),('Pot Holes Reported','Pot Holes Reported'),('Rodent Baiting','Rodent Baiting'),('Sanitation Code Complaints','Sanitation Code Complaints'),('Lights Out All','Lights Out All'),('Lights Out One','Lights Out One'),('Tree Debris','Tree Debris'),('Tree Trims','Tree Trims'))

    create_date = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=50, choices = STATUS_TYPE, default='Open')
    complete_date = models.DateTimeField(blank=True, null=True)
    service_req_type = models.CharField(max_length=100, choices=SERVICE_TYPE)
    street = models.CharField(max_length=100, blank=True, null=True, default='NULL')
    zip = models.IntegerField(blank=True, null=True, default='NULL')
    x = models.FloatField(blank=True, null=True, default='NULL')
    y = models.FloatField(blank=True, null=True, default='NULL')
    ward = models.IntegerField(blank=True, null=True, default='NULL')
    police_district = models.IntegerField(blank=True, null=True, default='NULL')
    community_area = models.IntegerField(blank=True, null=True, default='NULL')
    ssa = models.IntegerField(blank=True, null=True, default='NULL')
    latitude = models.FloatField(blank=True, null=True, default='NULL')
    longitude = models.FloatField(blank=True, null=True, default='NULL')
    location = models.CharField(max_length=100, blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'report'

class QueriesSub(models.Model):
    userID = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, db_column='userid')
    username = models.CharField(max_length=100, blank=True, null=True, db_column='username')
    subdate = models.DateTimeField(blank=True, null=True, db_column='subdate')

    class Meta:
        managed = False
        db_table = 'queriessub'

class ReportSub(models.Model):
    repID = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    userID = models.ForeignKey(User, models.DO_NOTHING, blank=True, null=True, db_column='userid')
    username = models.CharField(max_length=100, blank=True, null=True, db_column='username')
    subdate = models.DateTimeField(blank=True, null=True, db_column='subdate')

    class Meta:
        managed = False
        db_table = 'reportsub'

class AbandonedVehicles(models.Model):
    repid = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    licence_plate = models.CharField(max_length=500, blank=True, null=True, default='NULL')
    model = models.CharField(max_length=100, blank=True, null=True, default='NULL')
    color = models.CharField(max_length=100, blank=True, null=True, default='NULL')
    days_parked = models.IntegerField(blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'abandoned_vehicles'


class Activity(models.Model):
    repid = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    curr_activity = models.CharField(max_length=100, blank=True, null=True, default='NULL')
    most_recent_action = models.CharField(max_length=100, blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'activity'


class GarbageCarts(models.Model):
    repid = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    carts_delivered = models.IntegerField(blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'garbage_carts'


class GraffitiRemoval(models.Model):
    repid = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    surface_type = models.CharField(max_length=100, blank=True, null=True, default='NULL')
    location = models.CharField(max_length=100, blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'graffiti_removal'


class PotHoles(models.Model):
    repid = models.ForeignKey('Report', models.DO_NOTHING, db_column='repid', blank=True, null=True)
    potholes_filled = models.IntegerField(blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'pot_holes'


class RodentBaitings(models.Model):
    repid = models.ForeignKey(Report, models.DO_NOTHING, db_column='repid', blank=True, null=True)
    premises_baited = models.IntegerField(blank=True, null=True, default='NULL')
    premises_with_garbage = models.IntegerField(blank=True, null=True, default='NULL')
    premises_with_rats = models.IntegerField(blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'rodent_baitings'


class SanitationComplaints(models.Model):
    repid = models.ForeignKey(Report, models.DO_NOTHING, db_column='repid', blank=True, null=True)
    violation_nature = models.CharField(max_length=100, blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'sanitation_complaints'


class Trees(models.Model):
    repid = models.ForeignKey(Report, models.DO_NOTHING, db_column='repid', blank=True, null=True)
    location = models.CharField(max_length=100, blank=True, null=True, default='NULL')

    class Meta:
        managed = False
        db_table = 'trees'
