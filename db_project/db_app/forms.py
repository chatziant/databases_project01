from django.core.validators import RegexValidator
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

YEAR_CHOICES = ('2018','2017','2016','2015','2014','2013','2012','2011','2010','2009','2008', '2007','2006','2005','2004','2003','2002','2001',)

SERVICES_SUBSET = (('Alley Lights Out', 'Alley Lights Out',), ('Lights Out All','Lights Out All',), ('Lights Out One','Lights Out One'))

SERVICES_SUBSET2 = (('Abandoned Vehicle Complaint', 'Abandoned Vehicle Complaint',), ('Garbage Carts','Garbage Carts',), ('Graffiti Removal','Graffiti Removal',), ('Pot Holes Reported','Pot Holes Reported',), ('Rodent Baiting','Rodent Baiting',), ('Sanitation Code Complaints','Sanitation Code Complaints',), ('Tree Debris','Tree Debris'), ('Tree Trims','Tree Trims'), ('Other','Other'))

SERVICES = (('Abandoned Vehicle Complaint', 'Abandoned Vehicle Complaint',), ('Garbage Carts','Garbage Carts',), ('Graffiti Removal','Graffiti Removal',), ('Pot Holes Reported','Pot Holes Reported',), ('Rodent Baiting','Rodent Baiting',), ('Sanitation Code Complaints','Sanitation Code Complaints',), ('Tree Debris','Tree Debris'), ('Tree Trims','Tree Trims'), ('Alley Lights Out', 'Alley Lights Out',), ('Lights Out All','Lights Out All',), ('Lights Out One','Lights Out One'))

class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )

class Form1(forms.Form):
    day = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))

class Form2(forms.Form):
    start_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))
    end_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))

class Form3(forms.Form):
    service_type = forms.ChoiceField(widget=forms.Select, choices=SERVICES)
    start_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))
    end_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))

class Form4(forms.Form):
    number = forms.DecimalField(required=True, max_digits=3, min_value=1)

class Form5(forms.Form):
    latitude1 = forms.FloatField(required=True, min_value=41, max_value=42.02296045252183)
    latitude2 = forms.FloatField(required=True, min_value=41, max_value=42.02296045252183)
    longitude1 = forms.FloatField(required=True, min_value=-87.93957075671746, max_value=-86)
    longitude2 = forms.FloatField(required=True, min_value=-87.93957075671746, max_value=-86)
    day = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))

class ChooseReportTypeForm(forms.Form):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=SERVICES_SUBSET2)

class CreateReportForm(forms.Form):
    # create_date = datetime.datetime.now()
    # status = 'Open'
    # complete_date = NULL
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=SERVICES_SUBSET)
    street = forms.CharField(required=False, max_length=100)
    zip = forms.IntegerField(required=False, max_value=99999)
    x = forms.FloatField(required=False)
    y = forms.FloatField(required=False)
    ward = forms.IntegerField(required=False)
    police_district = forms.IntegerField(required=False)
    community_area = forms.IntegerField(required=False)
    ssa = forms.IntegerField(required=False)
    latitude = forms.FloatField(required=False, min_value=41, max_value=42.02296045252183)
    longitude = forms.FloatField(required=False, min_value=-87.93957075671746, max_value=-86)
    location = forms.CharField(required=False, max_length=100)

class CreateAbandonedVehicleReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Abandoned Vehicles Complaint','Abandoned Vehicles Complaint'),('','')), initial='Abandoned Vehicles Complaint' )
    licence_plate=forms.CharField(required=False, max_length=100)
    model=forms.CharField(required=False, max_length=100)
    color=forms.CharField(required=False, max_length=100)
    days_parked=forms.IntegerField(required=False)
    current_activity=forms.CharField(required=False, max_length=100)
    most_recent_action=forms.CharField(required=False, max_length=100)

class CreateGarbageCartReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Garbage Carts','Garbage Carts'),('','')), initial='Garbage Carts' )
    blank_carts_delivered=forms.IntegerField(required=False)
    current_activity=forms.CharField(required=False, max_length=100)
    most_recent_action=forms.CharField(required=False, max_length=100)

class CreateGraffitiReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Graffiti Removal','Graffiti Removal'),('','')), initial='Graffiti Removal' )
    surface_type=forms.CharField(required=False, max_length=100)
    graffiti_location=forms.CharField(required=False, max_length=100)

class CreatePotHolesReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Pot Holes Reported','Pot Holes Reported'),('','')), initial='Pot Holes Reported' )
    potholes_filled=forms.IntegerField(required=False)
    current_activity=forms.CharField(required=False, max_length=100)
    most_recent_action=forms.CharField(required=False, max_length=100)

class CreateRodentBaitingReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Rodent Baiting','Rodent Baiting'),('','')), initial='Rodent Baiting' )
    premises_baited=forms.IntegerField(required=False)
    premises_with_rats=forms.IntegerField(required=False)
    premises_with_garbage=forms.IntegerField(required=False)
    current_activity=forms.CharField(required=False, max_length=100)
    most_recent_action=forms.CharField(required=False, max_length=100)

class CreateSanitationReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Sanitation Code Complaint','Sanitation Code Complaint'),('','')), initial='Sanitation Code Complaint' )
    violation_nature=forms.CharField(required=False, max_length=100)

class CreateTreeDebrisReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Tree Debris','Tree Debris'),('','')), initial='Tree Debris' )
    debris_location=forms.CharField(required=False, max_length=100)
    current_activity=forms.CharField(required=False, max_length=100)
    most_recent_action=forms.CharField(required=False, max_length=100)

class CreateTreeTrimReportForm(CreateReportForm):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=(('Tree Trims','Tree Trims'),('','')), initial='Tree Trims' )
    trims_location=forms.CharField(required=False, max_length=100)

class SearchForm(forms.Form):
    service_req_type = forms.ChoiceField(widget=forms.Select, choices=SERVICES)
    street = forms.CharField(required=False, max_length=100)
    zip = forms.IntegerField(required=False, max_value=99999)
    start_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))
    end_date = forms.DateField(required=True, widget=forms.SelectDateWidget(years=YEAR_CHOICES))
