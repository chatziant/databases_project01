from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from db_app.models import function01,function02,function03,function04,function05,function06,function07,function08,function09,function10,function11,function12,Report,AbandonedVehicles,GarbageCarts,GraffitiRemoval,PotHoles,RodentBaitings,SanitationComplaints,Trees,Activity,QueriesSub,ReportSub
from django.contrib.auth.models import User
from db_app.forms import SignUpForm,Form1,Form2,Form3,Form4,Form5,ChooseReportTypeForm,CreateReportForm,CreateAbandonedVehicleReportForm,CreateGraffitiReportForm,CreatePotHolesReportForm,CreateTreeTrimReportForm,CreateSanitationReportForm,CreateTreeDebrisReportForm,CreateGarbageCartReportForm,CreateRodentBaitingReportForm,CreateAbandonedVehicleReportForm,SearchForm
import datetime


#Create your views here.
def index(request):
    return( render(request, 'index.html', {'title': "Index"}))

@login_required
def queries(request):
    return( render(request, 'queries.html', {'title': "Queries", 'username': request.user}))

def signup(request):
    if request.method == 'POST':
        form =  SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

@login_required
def func1(request):
    name="function01"
    if (request.method=="POST"):
        form = Form2(request.POST)
        if form.is_valid():
            start=form.cleaned_data['start_date']
            end=form.cleaned_data['end_date']

            description="Total requests per service type created from " + start.strftime('%d/%m/%Y') + " to " + end.strftime('%d/%m/%Y') + " sorted in descending order"

            cur_user = request.user
            foo = QueriesSub.objects.create(userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function01(start,end)}))
    else:
        form = Form2()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func2(request):
    name="function02"
    if (request.method=="POST"):
        form = Form3(request.POST)
        if form.is_valid():
            type=form.cleaned_data['service_type']
            start=form.cleaned_data['start_date']
            end=form.cleaned_data['end_date']

            description="Total requests for service type: " + "'" + type + "'" + " created from " + start.strftime('%d/%m/%Y') + " to " + end.strftime('%d/%m/%Y')

            cur_user = request.user
            foo = QueriesSub.objects.create(userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function02(type,start,end)}))
    else:
        form = Form3()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func3(request):
    name="function03"
    if (request.method=="POST"):
        form = Form1(request.POST)
        if form.is_valid():
            day=form.cleaned_data['day']

            description="Most common services per ZIP code for " + day.strftime('%d/%m/%Y')

            cur_user = request.user
            foo = QueriesSub.objects.create(userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function03(day)}))
    else:
        form = Form1()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func4(request):
    name="function04"
    if (request.method=="POST"):
        form = Form2(request.POST)
        if form.is_valid():
            start=form.cleaned_data['start_date']
            end=form.cleaned_data['end_date']

            description="Average completion time per service type from " + start.strftime('%d/%m/%Y') + " to " + end.strftime('%d/%m/%Y')

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function04(start,end)}))
    else:
        form = Form2()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func5(request):
    name="function05"
    if (request.method=="POST"):
        form = Form5(request.POST)
        if form.is_valid():
            lat1=form.cleaned_data['latitude1']
            lat2=form.cleaned_data['latitude2']
            lon1=form.cleaned_data['longitude1']
            lon2=form.cleaned_data['longitude2']
            day=form.cleaned_data['day']

            description="Most common service within box defined by latitude within [" + str(lat1) + "," + str(lat2) + "] and longitude within [" + str(lon1) + "," + str(lon2) + "] at " + day.strftime('%d/%m/%Y')

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function05(lat1,lat2,lon1,lon2,day)}))
    else:
        form = Form5()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})
#try those for test: '41.641662537751896','41.861662437751896','-87.81764092116586','-87.61865092116586'

@login_required
def func6(request):
    name="function06"
    if (request.method=="POST"):
        form = Form2(request.POST)
        if form.is_valid():
            start=form.cleaned_data['start_date']
            end=form.cleaned_data['end_date']

            description="Top5 SSAs with the most total number of requests per day from " + start.strftime('%d/%m/%Y') + " to " + end.strftime('%d/%m/%Y')

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function06(start,end)}))
    else:
        form = Form2()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func7(request):
    name="function07"
    description="Licence plates of vehicles involved in 'Abandoned Vehicles Complaints' more than once"
    return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function07()}))

@login_required
def func8(request):
    name="function08"
    description="Second most common color of vehicles involved in 'Abandoned Vehicles Complaints'"
    return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function08()}))

@login_required
def func9(request):
    name="function09"
    if (request.method=="POST"):
        form = Form4(request.POST)
        if form.is_valid():
            num=form.cleaned_data['number']

            description="'Rodent Baitings' request IDs with less than " + str(num) + " premises baited"

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function10(num)}))
    else:
        form = Form4()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func10(request):
    name="function10"
    if (request.method=="POST"):
        form = Form4(request.POST)
        if form.is_valid():
            num=form.cleaned_data['number']

            description="'Rodent Baitings' request IDs with less than " + str(num) + " premises with garbage"

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function10(num)}))
    else:
        form = Form4()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func11(request):
    name="function11"
    if (request.method=="POST"):
        form = Form4(request.POST)
        if form.is_valid():
            num=form.cleaned_data['number']

            description="'Rodent Baitings' request IDs with less than " + str(num) + " premises with rats"

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function10(num)}))
    else:
        form = Form4()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def func12(request):
    name="function12"
    if (request.method=="POST"):
        form = Form1(request.POST)
        if form.is_valid():
            day=form.cleaned_data['day']

            description="Police districts that have handled 'Pot Holes' requests with more than one potholes filled and also handled 'Rodent Baiting' requests with more than one number of premises baited on " + day.strftime('%d/%m/%Y')

            return( render(request, 'display.html', {'func_name': name, 'descr':description, 'res': function03(day)}))
    else:
        form = Form1()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def choose_incident(request):
    name="Select Incident to Report"
    if (request.method=="POST"):
        form = ChooseReportTypeForm(request.POST)
        if form.is_valid():
            service_req_type = form.cleaned_data.get('service_req_type')
            if (service_req_type=='Abandoned Vehicle Complaint'):
                return redirect('/report_vehicle/')
            elif (service_req_type=='Garbage Carts'):
                return redirect('/report_garbage/')
            elif (service_req_type=='Graffiti Removal'):
                return redirect('/report_graffiti/')
            elif (service_req_type=='Pot Holes Reported'):
                return redirect('/report_pot_hole/')
            elif (service_req_type=='Rodent Baiting'):
                return redirect('/report_rodent/')
            elif (service_req_type=='Sanitation Code Complaints'):
                return redirect('/report_sanitation/')
            elif (service_req_type=='Tree Debris'):
                return redirect('/report_debris/')
            elif (service_req_type=='Tree Trims'):
                return redirect('/report_trims/')
            else:
                return redirect('/create_report/')
    else:
        form = ChooseReportTypeForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def create_report(request):
    name="Incident Report"
    if (request.method=="POST"):
        form = CreateReportForm(request.POST)
        if form.is_valid():
            service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')

            rep = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type=service_req_type, street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})


@login_required
def report_vehicle(request):
    name="Abandoned Vehicle Report"
    if (request.method=="POST"):
        form = CreateAbandonedVehicleReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            licence_plate = form.cleaned_data.get('licence_plate')
            model = form.cleaned_data.get('model')
            color = form.cleaned_data.get('color')
            days_parked = form.cleaned_data.get('days_parked')
            current_activity = form.cleaned_data.get('current_activity')
            most_recent_action = form.cleaned_data.get('most_recent_action')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Abandoned Vehicle Complaint', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = AbandonedVehicles.objects.create(repid=rep1, licence_plate=licence_plate, model=model, color=color, days_parked=days_parked)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateAbandonedVehicleReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_garbage(request):
    name="Garbage Cart Report"
    if (request.method=="POST"):
        form = CreateGarbageCartReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            blank_carts_delivered = form.cleaned_data.get('blank_carts_delivered')
            current_activity = form.cleaned_data.get('current_activity')
            most_recent_action = form.cleaned_data.get('most_recent_action')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Garbage Carts', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = GarbageCarts.objects.create(repid=rep1, carts_delivered=blank_carts_delivered)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateGarbageCartReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_graffiti(request):
    name="Graffiti Report"
    if (request.method=="POST"):
        form = CreateGraffitiReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            surface_type = form.cleaned_data.get('surface_type')
            graffiti_location = form.cleaned_data.get('graffiti_location')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Graffiti Removal', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = GraffitiRemoval.objects.create(repid=rep1, surface_type=surface_type, location=graffiti_location)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateGraffitiReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_pot_hole(request):
    name="Pothole Report"
    if (request.method=="POST"):
        form = CreatePotHolesReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            potholes_filled = form.cleaned_data.get('potholes_filled')
            current_activity = form.cleaned_data.get('current_activity')
            most_recent_action = form.cleaned_data.get('most_recent_action')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Pot Holes Reported', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = PotHoles.objects.create(repid=rep1, potholes_filled=potholes_filled)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreatePotHolesReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_rodent(request):
    name="Rodent Baiting Report"
    if (request.method=="POST"):
        form = CreateRodentBaitingReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            premises_baited = form.cleaned_data.get('premises_baited')
            premises_with_rats = form.cleaned_data.get('premises_with_rats')
            premises_with_garbage = form.cleaned_data.get('premises_with_garbage')
            current_activity = form.cleaned_data.get('current_activity')
            most_recent_action = form.cleaned_data.get('most_recent_action')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Rodent Baiting', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = RodentBaitings.objects.create(repid=rep1, premises_baited=premises_baited, premises_with_rats=premises_with_rats, premises_with_garbage=premises_with_garbage)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateRodentBaitingReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_sanitation(request):
    name="Sanitation Violation Report"
    if (request.method=="POST"):
        form = CreateSanitationReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            violation_nature = form.cleaned_data.get('violation_nature')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Sanitation Code Complaints', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = SanitationComplaints.objects.create(repid=rep1, violation_nature=violation_nature)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateSanitationReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_debris(request):
    name="Tree Debris Report"
    if (request.method=="POST"):
        form = CreateTreeDebrisReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            debris_location = form.cleaned_data.get('debris_location')
            current_activity = form.cleaned_data.get('current_activity')
            most_recent_action = form.cleaned_data.get('most_recent_action')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Tree Debris', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = Trees.objects.create(repid=rep1, location=debris_location)

            rep3 = Activity.objects.create(repid=rep1, curr_activity=current_activity, most_recent_action=most_recent_action)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateTreeDebrisReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

@login_required
def report_trims(request):
    name="Tree Trims Report"
    if (request.method=="POST"):
        form = CreateTreeTrimReportForm(request.POST)
        if form.is_valid():
            # service_req_type = form.cleaned_data.get('service_req_type')
            street = form.cleaned_data.get('street')
            zip = form.cleaned_data.get('zip')
            x = form.cleaned_data.get('x')
            y = form.cleaned_data.get('y')
            ward = form.cleaned_data.get('ward')
            police_district = form.cleaned_data.get('police_district')
            community_area = form.cleaned_data.get('community_area')
            ssa = form.cleaned_data.get('ssa')
            latitude = form.cleaned_data.get('latitude')
            longitude = form.cleaned_data.get('longitude')
            location = form.cleaned_data.get('location')
            trims_location = form.cleaned_data.get('trims_location')

            rep1 = Report.objects.create(create_date=datetime.datetime.now(), status='Open', service_req_type='Tree Trims', street=street, zip=zip, x=x, y=y, ward=ward, police_district=police_district, community_area=community_area, ssa=ssa, latitude=latitude, longitude=longitude, location=location)

            rep2 = Trees.objects.create(repid=rep1, location=trims_location)

            cur_user=request.user
            foo = ReportSub.objects.create(repID=rep1, userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return redirect('/queries/')
    else:
        form = CreateTreeTrimReportForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})

"""returns all data relevant to a service request type category according to
either the street and/or zipcode for a specified date range"""
@login_required
def search(request):
    name="Search for incidents"
    if (request.method=="POST"):
        form = SearchForm(request.POST)
        if form.is_valid():
            service_type=form.cleaned_data.get('service_req_type')
            street=form.cleaned_data.get('street')
            zip=form.cleaned_data.get('zip')
            start=form.cleaned_data.get('start_date')
            end=form.cleaned_data.get('end_date')

            cur=connection.cursor();

            sql_q="SELECT r.id as rid, r.create_date, r.status, r.complete_date, r.street, r.zip, r.x, r.y, r.ward, r.police_district, r.community_area, r.SSA, r.latitude, r.longitude"

            if(service_type=='Abandoned Vehicle Complaint'):
                sql_q+=",av.licence_plate, av.model, av.color, av.days_parked, activity.curr_activity, activity.most_recent_action FROM report AS r INNER JOIN abandoned_vehicles AS av ON r.id=av.repID INNER JOIN activity ON r.id=activity.repID WHERE r.service_req_type='Abandoned Vehicle Complaint' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Alley Lights Out'):
                sql_q+=" FROM report AS r WHERE r.service_req_type='Alley Lights Out' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Garbage Carts'):
                sql_q+=",g.carts_delivered, activity.curr_activity, activity.most_recent_action FROM report AS r INNER JOIN garbage_carts AS g ON r.id=g.repID INNER JOIN activity ON r.id=activity.repID WHERE r.service_req_type='Garbage Carts' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Graffiti Removal'):
                sql_q+=",gr.surface_type, gr.location FROM report AS r INNER JOIN graffiti_removal AS gr ON r.id=gr.repID WHERE r.service_req_type='' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Pot Holes Reported'):
                sql_q+=",p.potholes_filled, activity.curr_activity, activity.most_recent_action FROM report AS r INNER JOIN pot_holes AS p ON r.id=p.repID INNER JOIN activity ON r.id=activity.repID WHERE r.service_req_type='Pot Holes Reported' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Rodent Baiting'):
                sql_q+=",rb.premises_baited, rb.premises_with_rats, rb.premises_with_garbage, activity.curr_activity, activity.most_recent_action FROM report AS r INNER JOIN rodent_baitings AS rb ON r.id=rb.repID INNER JOIN activity ON r.id=activity.repID WHERE r.service_req_type='Rodent Baiting' AND  r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Sanitation Code Complaints'):
                sql_q+=",s.violation_nature FROM report AS r INNER JOIN sanitation_complaints AS s ON r.id=s.repID WHERE r.service_req_type='Sanitation Code Complaints' AND  r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Lights Out All'):
                sql_q+=" FROM report AS r WHERE  r.service_req_type='Lights Out All' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Lights Out One'):
                sql_q+=" FROM report AS r WHERE  r.service_req_type='Lights Out One' AND r.create_date>=%s AND r.create_date<=%s"
            elif(service_type=='Tree Debris'):
                sql_q+=",t.location, activity.curr_activity, activity.most_recent_action FROM report AS r INNER JOIN trees AS t ON r.id=t.repID INNER JOIN activity ON r.id=activity.repID WHERE r.service_req_type='Tree Debris' AND r.create_date>=%s AND r.create_date<=%s"
            else:
                sql_q+=",t.location FROM report AS r INNER JOIN trees AS t ON r.id=t.repID WHERE r.service_req_type='Tree Trims' AND r.create_date>=%s AND r.create_date<=%s"

            if (street!='' and zip!=None):
                sql_q += " AND street=%s AND zip=%s ORDER BY r.create_date;"
                cur.execute(sql_q, [start, end, street, zip])
            elif (street!=''):
                sql_q += " AND street=%s ORDER BY r.create_date;"
                cur.execute(sql_q, [start, end, street])
            elif (zip!=None):
                sql_q += " AND zip=%s ORDER BY r.create_date;"
                cur.execute(sql_q, [start, end, zip])
            else:
                sql_q += " ORDER BY r.create_date;"
                cur.execute(sql_q, [start, end])
            res = cur.fetchall()
            cur.close()

            cur_user = request.user
            foo = QueriesSub.objects.create(userID=cur_user, username=cur_user, subdate=datetime.datetime.now())

            return( render(request, 'search_results.html', {'func_name': name, 'type':service_type, 'result':res }))
    else:
        form = SearchForm()
    return render(request, 'post_edit.html', {'func_name': name, 'form': form})
