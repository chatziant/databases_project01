from django.contrib import admin
from db_app.models import Report, AbandonedVehicles, Activity, GarbageCarts, GraffitiRemoval, PotHoles, RodentBaitings, SanitationComplaints, Trees, QueriesSub,ReportSub

# Register your models here
@admin.register(Report)
class SuperReport(admin.ModelAdmin):
    list_display=('create_date', 'service_req_type', 'complete_date', 'status')


@admin.register(AbandonedVehicles)
class SuperAbandonedVehicles(admin.ModelAdmin):
    list_display=('repid', 'licence_plate', 'color', 'days_parked')


@admin.register(Activity)
class SuperActivity(admin.ModelAdmin):
    list_display=('repid', 'curr_activity', 'most_recent_action')


@admin.register(GarbageCarts)
class SuperGarbageCarts(admin.ModelAdmin):
    list_display=('repid', 'carts_delivered')


@admin.register(GraffitiRemoval)
class SuperGraffitiRemoval(admin.ModelAdmin):
    list_display=('repid', 'surface_type', 'location')

@admin.register(PotHoles)
class SuperPotHoles(admin.ModelAdmin):
    list_display=('repid', 'potholes_filled')

@admin.register(RodentBaitings)
class SuperRodentBaitings(admin.ModelAdmin):
    list_display=('repid', 'premises_baited', 'premises_with_garbage', 'premises_with_rats')

@admin.register(SanitationComplaints)
class SuperSanitationComplaints(admin.ModelAdmin):
    list_display=('repid', 'violation_nature')

@admin.register(Trees)
class SuperTrees(admin.ModelAdmin):
    list_display=('repid', 'location')

@admin.register(QueriesSub)
class SuperQueriesSub(admin.ModelAdmin):
    list_display=('userID', 'username', 'subdate')

@admin.register(ReportSub)
class SuperReportSub(admin.ModelAdmin):
    list_display=('userID', 'username', 'repID', 'subdate')

# admin.site.register(QueriesSub)
# admin.site.register(ReportSub)
# admin.site.register(Report)
# admin.site.register(AbandonedVehicles)
# admin.site.register(Activity)
# admin.site.register(GarbageCarts)
# admin.site.register(GraffitiRemoval)
# admin.site.register(PotHoles)
# admin.site.register(RodentBaitings)
# admin.site.register(SanitationComplaints)
# admin.site.register(Trees)
