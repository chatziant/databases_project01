"""db_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from db_app import views

urlpatterns = [
    path('', views.index, name='index'),
    path('signup/', views.signup, name='signup'),
    path('queries/', views.queries, name='queries'),
    path('choose_incident/', views.choose_incident, name='choose_incident'),
    path('create_report/', views.create_report, name='create_report'),
    path('report_vehicle/', views.report_vehicle, name='report_vehicle'),
    path('report_garbage/', views.report_garbage, name='report_garbage'),
    path('report_graffiti/', views.report_graffiti, name='report_graffiti'),
    path('report_pot_hole/', views.report_pot_hole, name='report_pot_hole'),
    path('report_rodent/', views.report_rodent, name='report_rodent'),
    path('report_sanitation/', views.report_sanitation, name='report_sanitation'),
    path('report_debris/', views.report_debris, name='report_debris'),
    path('report_trims/', views.report_trims, name='report_trims'),
    path('search/', views.search, name='search'),
    path('func1/', views.func1, name='func1'),
    path('func2/', views.func2, name='func2'),
    path('func3/', views.func3, name='func3'),
    # path('func4/', views.func4, name='func4'),
    # path('func5/', views.func5, name='func5'),
    # path('func6/', views.func6, name='func6'),
    # path('func7/', views.func7, name='func7'),
    # path('func8/', views.func8, name='func8'),
    # path('func9/', views.func9, name='func9'),
    # path('func10/', views.func10, name='func10'),
    # path('func11/', views.func11, name='func11'),
    # path('func12/', views.func12, name='func12'),
    #
]
