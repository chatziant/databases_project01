## Database Managment Systems 2018-19
### Project 01

This is a Django project for a database manipulation web application for the  
postgraduate 'Databases Management Systems' course of the department of Informatics 
and Telecommunications from the National and Kapodestrian University of Athens.

In order to run the applications the following steps are required:
1. Download the csv files from [here](https://www.kaggle.com/chicago/chicago-311-service-requests)
2. Create a postgresql database named *cs311* and a user named *test_user* with 
   password: *'password'*, and grant him all privileges for the database
    ```
    CREATE DATABASE cs311;
    CREATE USER test_user WITH PASSWORD 'password';
    GRANT ALL PRIVILEDGES ON cs311 TO test_user;
    ```
3. Create a project directory, create a virtual environment, download django and psycopg2 and clone the project files. 
   Make sure csv files are there too.
    ```
   mkdir project
   cd project
   virtualenv -p python3 env
   source env/bin/activate
   pip install django
   pip install psycopg2-binary
   ```
4. Clone the project:
   ```
   git clone https://gitlab.com/chatziant/databases_project01
   ```
5. In order to use the app with a full database the data from the csv files must be entered to the database. 
   This is done offline with some python files located at the db_create_and_fill directory 
   ```
   cd databases_project01/project01/db_create_and_fill/db_config
   python db_create_tables.py
   ```
   The program connects with the database and parses each csv file filling with data the coresponding fields of the relation tables, so this will take a while...
6. After the database is filled with data the application can be used. To do that, first migrate the table changes and then start the server.
   ```
   cd databases_project01/project01/db_project/db_project
   python manage.py makemigrations
   python manage.py migrate
   python manage.py createsuperuser #optional
   python manage.py runserver
   ```
7. Open a browser to `http://localhost:8000`
8. Test the app 

*Warning: The app is ugly! I'm not a web developper and 
I don't intend to be one, so please have some undestanding :)*